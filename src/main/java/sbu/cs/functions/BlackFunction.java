package sbu.cs.functions;

public class BlackFunction {
    public static String gatValue(String value, int func)
    {
        String answer = "";

        switch (func) {
            case 1 :
                answer = func1(value);
                break;

            case 2 :
                answer = func2(value);
                break;

            case 3 :
                answer = func3(value);
                break;

            case 4 :
                answer = func4(value);
                break;

            case 5 :
                answer = func5(value);
                break;
        }

        return answer;
    }


    private static String func1(String value) {
        String answer = new StringBuilder(value).reverse().toString();

        return answer;
    }

    private static String func2(String value) {
        String answer = "";
        for (char character : value.toCharArray()) {
            answer += character + "" + character + "";
        }

        return answer;
    }

    private static String func3(String value) {
        String answer = value + "" + value + "";
        return answer;
    }


    private static String func4(String value)
    {
        String answer = value.charAt(value.length() - 1) + new StringBuilder(value).deleteCharAt(value.length() - 1).toString();
        return answer;
    }

    private static String func5(String value)
    {
        String answer = "";

        for (char character : value.toCharArray()) {
            answer += (char)((('z' - character) + 'a'));
        }

        return answer;
    }
}
