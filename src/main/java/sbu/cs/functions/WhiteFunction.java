package sbu.cs.functions;

public class WhiteFunction {
    public static String getValue(String value1, String value2, int funcNumber)
    {
        String answer = "";

        switch (funcNumber)
        {
            case 1 :
                answer = func1(value1, value2);
                break;

            case 2 :
                answer = func2(value1, value2);
                break;

            case 3 :
                answer = func3(value1, value2);
                break;

            case 4 :
                answer = func4(value1, value2);
                break;

            case 5 :
                answer = func5(value1, value2);
                break;
        }

        return answer;
    }

    private static String func1(String value1, String value2) {
        String answer = "";
        int minLength = Math.min(value1.length(), value2.length());

        for (int i = 0; i < minLength; i++) {
            answer += (value1.charAt(i) + "" + value2.charAt(i));
        }
        answer += value1.substring(minLength) + value2.substring(minLength);
        return answer;
    }

    private static String func2(String value1, String value2) {
        String answer = value1 + new StringBuilder(value2).reverse().toString();

        return answer;
    }

    private static String func3(String value1, String value2) {
        String answer = func1(value1, new StringBuilder(value2).reverse().toString());

        return answer;
    }

    private static String func4(String value1, String value2) {
        String answer = "";
        if (value1.length() % 2 == 0)
            answer = value1;
        else
            answer = value2;

        return answer;
    }

    private static String func5(String value1, String value2)
    {
        String answer = "";
        int minLength = Math.min(value1.length(), value2.length());

        for (int i = 0; i < minLength; i++) {
            char character = (char) (((value1.charAt(i) - 'a' + value2.charAt(i) - 'a') % 26) + 'a');
            answer += character;
        }
        answer += value1.substring(minLength) + value2.substring(minLength);
        return answer;
    }

}
