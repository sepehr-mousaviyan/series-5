package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for (int i = 0; i < size - 1; i++) {
            int minIndex= i;

            for (int j = i + 1; j < size; j++)
                if (arr[j] < arr[minIndex])
                    minIndex = j;

            //Swap the values
//            arr[minIndex] = arr[minIndex] + arr[i];
//            arr[i] = arr[minIndex] - arr[i];
//            arr[minIndex] = arr[minIndex] - arr[i];
            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int j = 1; j < size; j++) {
            int key = arr[j];
            int i = j-1;
            while ( (i > -1) && ( arr[i] > key ) ) {
                arr[i+1] = arr[i];
                i--;
            }
            arr[i+1] = key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size < 2) {
            return arr;
        }
        int mid = size / 2;
        int[] l = new int[mid];
        int[] r = new int[size - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = arr[i];
        }
        for (int i = mid; i < size; i++) {
            r[i - mid] = arr[i];
        }
        mergeSort(l, mid);
        mergeSort(r, size - mid);

        merge(arr, l, r, mid, size - mid);
        return arr;
    }

    public static void merge(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }
    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int begin = 0;
        int end = arr.length - 1;

        while(begin <= end)
        {
            int mid = (begin + end) / 2;

            if(arr[mid] == value)
                return mid;
            if(arr[mid] > value)
                end = mid - 1;
            else
                begin = mid + 1;
        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        int index = recursiveStructure(arr, 0, arr.length, value);
        return index;
    }

    public int recursiveStructure(int[] arr, int begin, int end, int value) {
        if (end >= begin) {
            int mid = (begin + end) / 2;

            if (arr[mid] == value)
                return mid;
            if (arr[mid] > value)
                return recursiveStructure(arr, begin, mid -1, value);

            return recursiveStructure(arr, mid + 1, end, value);
        }

        return -1;
    }
}
