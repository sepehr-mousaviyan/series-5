package sbu.cs;

import sbu.cs.Pixels.*;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */

    Pixel[][] pixels;
    int [][] arr;
    int n;

    public String main(int n, int[][] arr, String input) {
        this.arr = arr;
        this.n = n;
        pixels = new Pixel[n][n];

        setValues();
        ((GreenPixel)pixels[0][0]).setInput(input);
        String Answer = outPut();

        return Answer;
    }

    private void setValues() {
        setFirstRow();
        setMiddleRows();
        setLastRow();
    }

    private void setFirstRow() {
        for (int i = 0; i < n - 1; i++) {
            pixels[0][i] = new GreenPixel(arr[0][i]);
        }
        pixels[0][n - 1] = new YellowPixel(arr[0][n - 1]);
    }

    private void setMiddleRows()
    {
        for (int i = 1; i < n - 1; i++) {
            for (int j = 0; j < n; j++) {
                if (j == 0) {
                    pixels[i][j] = new GreenPixel(arr[i][j]);
                    continue;
                }
                if (j == n -  1) {
                    pixels[i][j] = new PinkPixel(arr[i][j]);
                    continue;
                } else
                    pixels[i][j] = new BluePixel(arr[i][j]);
            }
        }
    }

    private void setLastRow()
    {
        pixels[n - 1][0] = new YellowPixel(arr[n - 1][0]);

        for (int i = 1; i < n; i++) {
            pixels[n - 1][i] = new PinkPixel(arr[n - 1][i]);
        }
    }

    private String outPut() {
        firstRowOut();
        middleRowOut();
        lastRowOut();
        return ((PinkPixel)pixels[n - 1][n - 1]).getAnswer();

    }

    private void firstRowOut()
    {
        for (int i = 0; i < n; i++) {
            if (i == n - 1) {
                String answer = ((YellowPixel)pixels[0][i]).getAnswer();
                ((PinkPixel)pixels[1][i]).setUpInput(answer);
                continue;
            }

            if (i == 0) {
                String answer = ((GreenPixel)pixels[0][0]).getAnswer();
                ((GreenPixel)pixels[0][i + 1]).setInput(answer);
                ((GreenPixel)pixels[1][i]).setInput(answer);
                continue;
            }


            String answer = ((GreenPixel)pixels[0][i]).getAnswer();

            if (pixels[0][i + 1] instanceof GreenPixel) {
                ((GreenPixel)pixels[0][i + 1]).setInput(answer);
            } else {
                ((YellowPixel)pixels[0][i + 1]).setInput(answer);
            }

            ((BluePixel)pixels[1][i]).setUpInput(answer);

        }
    }

    private void middleRowOut() {
        for (int i = 1; i < n - 1; i++) {
            for (int j = 0; j < n; j++) {
                if (j == 0) {
                    String answer = ((GreenPixel) pixels[i][j]).getAnswer();

                    ((BluePixel) pixels[i][j + 1]).setLeftAnswer(answer);
                    if (pixels[i + 1][j] instanceof GreenPixel) {
                        ((GreenPixel) pixels[i + 1][j]).setInput(answer);
                    } else {
                        ((YellowPixel) pixels[i + 1][j]).setInput(answer);
                    }
                } else if (j == n - 1) {
                    String answer = ((PinkPixel) pixels[i][j]).getAnswer();

                    ((PinkPixel) pixels[i + 1][j]).setUpInput(answer);
                } else {
                    String rightAnswer = ((BluePixel) pixels[i][j]).getRightAnswer();
                    String downAnswer = ((BluePixel) pixels[i][j]).getDownAnswer();

                    if (pixels[i][j + 1] instanceof BluePixel) {
                        ((BluePixel) pixels[i][j + 1]).setLeftAnswer(rightAnswer);
                    } else if (pixels[i][j + 1] instanceof PinkPixel) {
                        ((PinkPixel) pixels[i][j + 1]).setLeftInput(downAnswer);
                    }
                    if (pixels[i + 1][j] instanceof BluePixel) {
                        ((BluePixel) pixels[i + 1][j]).setUpInput(downAnswer);
                    }
                    if (pixels[i + 1][j] instanceof PinkPixel) {
                        ((PinkPixel) pixels[i + 1][j]).setUpInput(downAnswer);
                    }
                }
            }
        }
    }


    private void lastRowOut() {
        for(int i = 0; i < n - 1; i++) {
            if (i == 0) {
                String out = ((YellowPixel)pixels[n - 1][i]).getAnswer();
                ((PinkPixel)pixels[n - 1][i + 1]).setLeftInput(out);
                continue;
            }
            String answer = ((PinkPixel)pixels[n - 1][i]).getAnswer();

            ((PinkPixel)pixels[n - 1][i + 1]).setLeftInput(answer);
        }
    }
    
}
