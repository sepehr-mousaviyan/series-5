package sbu.cs.Pixels;

import sbu.cs.functions.BlackFunction;

public class GreenPixel extends Pixel{
    private String input, answer;

    public GreenPixel(int funcNumber) {
        super(funcNumber);
    }

    public void setInput(String input) {
        this.input = input;
        this.answer = BlackFunction.gatValue(input, super.getFuncNumber());
    }

    public String getAnswer() {
        return answer;
    }

}


