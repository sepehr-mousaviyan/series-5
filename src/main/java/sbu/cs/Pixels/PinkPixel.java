package sbu.cs.Pixels;

import sbu.cs.functions.WhiteFunction;

public class PinkPixel extends Pixel{
    private String upSideInput = "",leftSideInput = "", answer;

    public PinkPixel(int funcNumber) {
        super(funcNumber);
    }

    public void setLeftInput(String leftInput) {
        this.leftSideInput = leftInput;
        if (this.leftSideInput.length() != 0 && this.upSideInput.length() != 0) {
            this.answer = WhiteFunction.getValue(leftSideInput, upSideInput, super.getFuncNumber());
        }
    }

    public void setUpInput(String upInput) {
        this.upSideInput = upInput;
        if (this.leftSideInput.length() != 0 && this.upSideInput.length() != 0) {
            this.answer = WhiteFunction.getValue(leftSideInput, upSideInput, super.getFuncNumber());
        }
    }

    public String getAnswer() {
        return answer;
    }
}
