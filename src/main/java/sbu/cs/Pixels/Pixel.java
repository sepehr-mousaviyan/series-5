package sbu.cs.Pixels;

public class Pixel {

    private int funcNumber;

    public Pixel(int funcNumber) {
        this.funcNumber = funcNumber;
    }

    public int getFuncNumber() {
        return funcNumber;
    }
    //No need for setter
}
