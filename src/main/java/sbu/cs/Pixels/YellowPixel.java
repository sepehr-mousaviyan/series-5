package sbu.cs.Pixels;

import sbu.cs.functions.BlackFunction;

public class YellowPixel extends Pixel{

    public YellowPixel(int funcNumber) {
        super(funcNumber);
    }

    private String input, answer;

    public String getAnswer() {
        return answer;
    }

    public void setInput(String input) {
        this.input = input;
        this.answer = BlackFunction.gatValue(input, super.getFuncNumber());
    }
}
