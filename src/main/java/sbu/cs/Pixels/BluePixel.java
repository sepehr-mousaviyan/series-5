package sbu.cs.Pixels;

import sbu.cs.functions.BlackFunction;

public class BluePixel extends Pixel {

    public BluePixel(int funcNumber) {
        super(funcNumber);
    }

    private String leftSideInput ,upSideInput, rightSideAnswer, downSideAnswer;

    public void setUpInput(String upInput) {
        this.upSideInput = upInput;
        this.downSideAnswer = BlackFunction.gatValue(upSideInput, super.getFuncNumber());
    }

    public String getDownAnswer() {

        return downSideAnswer;
    }

    public String getRightAnswer() {

        return rightSideAnswer;
    }

    public void setLeftAnswer(String leftInput) {
        this.leftSideInput = leftInput;
        this.rightSideAnswer = BlackFunction.gatValue(leftSideInput, super.getFuncNumber());
    }
}
